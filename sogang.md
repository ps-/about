# Sogang Lecture 2023-04-05

## References

- Deepfake Detection - https://hai.stanford.edu/news/using-ai-detect-seemingly-perfect-deep-fake-videos
- Pentagon SOCOM - https://theintercept.com/2023/03/06/pentagon-socom-deepfake-propaganda/
- Tom Cruise TikTok deepfake - https://www.tiktok.com/@deeptomcruise/video/6932166297996233989
- Moon Jae-in approval rating - http://www.realmeter.net/wp-content/uploads/2019/12/%EB%A6%AC%EC%96%BC%EB%AF%B8%ED%84%B0tbs%ED%98%84%EC%95%88%ED%86%B5%EA%B3%84%ED%91%9C12%EC%9B%943%EC%A3%BC_2019%EB%85%8412%EC%9B%94%EB%AC%B8%EC%9E%AC%EC%9D%B8%EB%8C%80%ED%86%B5%EB%A0%B9.%EA%B5%AD%EC%A0%95%EC%A7%80%EC%A7%80%EB%8F%84%EB%B9%84%EA%B5%90%EC%B5%9C%EC%A2%85.pdf
- GMX Protocol Code - https://github.com/gmx-io/gmx-contracts
- Code in legal contracts - https://law.mit.edu/pub/writinginsign/release/1
- PoH - https://github.com/joss-aztec/poh-semaphore
- Totally Under Control (documentary film) - https://en.wikipedia.org/wiki/Totally_Under_Control
- US Literacy Rates - https://nces.ed.gov/pubs2019/2019179/index.asp
- COVID19 death data - https://covid19.who.int/data

## Personal Interests

- Startups, distributed computing and ledgers, geoeconomics, whiskey


## Social

- My Nostr public key - npub1pqw9vq6n0mgea9lpqumv672ms2q8jjscl2ehe2humn4hrp3lurxst3wedy
- My Nostr id - setjmp@iris.to


